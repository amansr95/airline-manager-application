let MongoClient = require('mongodb').MongoClient,
    url = 'mongodb://Airline-Manager-App:Qwerty1234@cluster0-shard-00-00-bpvit.mongodb.net:27017,cluster0-shard-00-01-bpvit.mongodb.net:27017,cluster0-shard-00-02-bpvit.mongodb.net:27017/AirlineManager?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';

exports.getManufacturers = (req, res) => {
    MongoClient.connect(url, (err, client) => {
        if(err) console.log(err);
        console.log('Connected....');
        const collection = client.db('AirlineManager').collection('Manufacturers');
        collection.find({}).toArray((error, result) => {
            if(error){
                return res.status(500).json(error);
            }
            res.send(result);
        });
    })
}

exports.getAircrafts = (manufacturer, res) => {
    MongoClient.connect(url, (err, client) => {
        if(err)console.log(err);
        console.log('Connected....'+manufacturer);
        const collection = client.db('AirlineManager').collection('Aircrafts');
        collection.find({Manufacturer: manufacturer}).toArray((error, result) => {
            if(error)
            return res.status(500).json(error);

            res.send(result);
        });
    })
}