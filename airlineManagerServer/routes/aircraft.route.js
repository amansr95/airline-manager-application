module.exports = (app) => {
    let aircraftController = require('../controllers/aircraft.controller');
    
    app.route('/manufacturers')
    .get(aircraftController.manufacturers);
    
    app.route('/aircrafts/:manufacturer')
    .get(aircraftController.aircrafts);
}