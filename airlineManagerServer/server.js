let express = require('express'),
    cors = require('cors'),
    port = process.env.PORT || 8080,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');

mongoose.connect('mongodb://Airline-Manager-App:Qwerty1234@cluster0-shard-00-00-bpvit.mongodb.net:27017,cluster0-shard-00-01-bpvit.mongodb.net:27017,cluster0-shard-00-02-bpvit.mongodb.net:27017/AirlineManager?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true', {
    useNewUrlParser: true
});

mongoose.Promise = global.Promise;

let app = express();

app.use(cors());
app.use(bodyParser.json());

let initApp = require('./app');
initApp(app);

app.listen(port);
console.log('Airline Manager App started on '+ port);