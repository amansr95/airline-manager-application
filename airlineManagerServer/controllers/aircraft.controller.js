let aircraftService = require('../services/aircraft.service');

exports.manufacturers = (req, res) => {
    aircraftService.getManufacturers(req, res);
}

exports.aircrafts = (req, res) => {
    let manufacturer = req.params.manufacturer;
    aircraftService.getAircrafts(manufacturer, res);
}