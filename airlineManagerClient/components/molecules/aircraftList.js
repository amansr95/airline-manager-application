import React from 'react';
import { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import axios from 'axios';
import AircraftRow from '../atoms/aircraftRow';
import { Paper, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

class AircraftList extends Component {
    constructor(){
        super();
        this.state = {
            aircrafts: []
        };
    }

    componentDidMount = () => {
        const {match: {params}} = this.props;

        axios.get(`http://localhost:8080/aircrafts/${params.manufacturer}`)
        .then(response => {
            let aircrafts = response.data.map(aircraft => {
                return(
                    <AircraftRow 
                        key={aircraft._id}
                        data={aircraft}
                    />
                )
            })
            this.setState({
                aircrafts: aircrafts
            })
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <Paper className={classes.root}>
                <style jsx>{`
                
                `}
                </style>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">Aircraft</TableCell>
                            <TableCell align="left">Passenger Capacity</TableCell>
                            <TableCell align="left">Operating Range</TableCell>
                            <TableCell align="left">Cost Excluding Tax</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.aircrafts}
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

AircraftList.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AircraftList);