import React from 'react';
import { Component } from 'react';
import axios from 'axios';
import Manufacturer from '../atoms/manufacturer';

class ManufacturerList extends Component {
    constructor(){
        super();
        this.state = {
            manufacturers: []
        };
    }

    componentDidMount(){
        axios.get('http://localhost:8080/manufacturers')
        .then( response =>{
            let manufacturers = response.data.map((manufacturer) => {
                return(
                    <Manufacturer
                        key={manufacturer.SNo}
                        imgPath={manufacturer.Image}
                        manufacturerName={manufacturer.Name}
                        manufacturerLocation={manufacturer.City+' '+ manufacturer.Country}
                        />
                )
            })
            this.setState({manufacturers: manufacturers});
        })
    }

    render() {
        return (
            <ul className='manufacturers'>
                <style jsx>{
                    `
                    .manufacturers {
                        display: flex;
                        flex-wrap: wrap;
                        width: 100%;
                        list-style-type: none;
                        padding: 0px;
                    }
                    `
                }
                </style>
                {this.state.manufacturers}
            </ul>
        )
    }
}
export default ManufacturerList;