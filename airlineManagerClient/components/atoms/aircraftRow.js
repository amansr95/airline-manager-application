import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';


const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
});

const AircraftRow = (props) => {
    let cost = formatter.format(props.data.Cost);
    return (
        <TableRow key={props.data._id}>
            <style jsx>{`
                .image {
                    max-width: 30%;
                    height: 30px;
                    padding: 10px;
                    padding-bottom: 0;
                }
            `}</style>
            <TableCell component="th" scope="row" align="left">
                <img src={`../assets/img/aircraft/${props.data.ImageURL}`} className="image"/>
                {props.data.Manufacturer+" "+props.data.Model}
            </TableCell>
            <TableCell align="left">{props.data.SeatCapacity}</TableCell>
            <TableCell align="left">{props.data.Range}</TableCell>
            <TableCell align="left">{cost}</TableCell>
        </TableRow>
    );
};

export default AircraftRow;
