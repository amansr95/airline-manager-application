import React  from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom'

const styles = {
    card: {
            maxWidth: 350,
            maxHeight: 300,
            width: '27%',
            padding: '10px',
            margin: '25px',
            border: '#13060a',
            borderStyle: 'solid',
            float: 'left'
        },
    media: {
                height: 250
            }   
};

const Manufacturer = (props) => {
    const { classes } = props;
    return(
        <Card className={classes.card}>
        <Link to={`/aircrafts/${props.manufacturerName}`}>
            <CardActionArea>
                <CardMedia 
                    className={classes.media}
                    image={props.imgPath}
                    title={props.manufacturerName}
                />
                <Typography gutterBottom variant="h6" component="h2">
                    {props.manufacturerName+" "+props.manufacturerLocation}
                </Typography>
            </CardActionArea>
        </Link>
        </Card>
    );
};

Manufacturer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Manufacturer);