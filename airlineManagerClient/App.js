import React, { Component } from 'react';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import './app.css';
import ManufacturerList from './components/molecules/manufacturerList';
import AircraftList from './components/molecules/aircraftList';

let Locations = Router.Locations;
let Location = Router.Location;
export default class App extends Component {
  
  render() {
    return (
        <Router>
          <div>
            <Route path="/" exact component={ManufacturerList} />
            <Route path="/aircrafts/:manufacturer" component={AircraftList} />
          </div>
        </Router>
    );
  }
}
